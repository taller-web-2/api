const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const routes = require('./lib/routes/index');

const app = express();

function connectMongoose() {
    const mongoose = require('mongoose');
    mongoose.Promise = Promise;
    return mongoose.connect(`${process.env.MONGO_DB}`, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false
    });
}

function initialize() {
    app.use(cors());
    app.use(morgan('dev'));
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }))

    // Aca van todas las rutas que vayamos a usar
    Object.keys(routes).forEach((key) => {
        app.use(`/api/${key}`, routes[key]);
    });
    return app;
}


module.exports = {
    initialize,
    connectMongoose
};
