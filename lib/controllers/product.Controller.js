const productController = {}

const Product = require ('../models/Product');

productController.getTodosProductos=async(req, res) => {
    return await Product.find({})
        .then((todosProductos)=>{ return res.status(200).json(todosProductos)})
        .catch(()=>{
            return res.status(500).json({
                message: 'Le pifiaste amigo', 
                code: 500
            });
        })
}

productController.getProducto=async(req, res) => {
    if (!req.params.id) {
        return res.status(400).json({
            message: 'El id es requerido',
            code: 'missing_params'
        });
    }
    
    return await Product.findOne({_id: req.params.id})
        .then((producto)=>{ 
            return res.status(200).json(producto)
        })
        .catch(()=>{
            return res.status(500).json({
                message: 'No existe el producto', 
                code: 500
            });
        })
}

productController.newProducto=async(req, res)=>{
    var product = new Product({
        name: req.body.name,
        description: req.body.description,
        classification: req.body.classification,
        price:  req.body.price,
    })
    return product.save()
    .then(() => {
        return res.status(200).json({
            message: 'Producto creado con exito',
            code:200,
        });
    })
    .catch((error)=>{
        console.log(error);
    })
}

module.exports = productController;