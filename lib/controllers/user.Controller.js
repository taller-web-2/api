const userController = {}

const User = require ('../models/User');

userController.validateUserByToken=async(req, res) => {
    if (!req.params.tokenValidator) {
        return res.status(400).json({
            message: 'El token es requerido',
            code: 'missing_params'
        });
    }

    return await User.findOneAndUpdate({tokenValidator: req.params.tokenValidator}, {$set:{validatedMail: true}})
    .then((user)=>{
        return res.status(200).json({
            message: 'Validado con exito', 
            code: 200
        })})
    .catch(()=>{
        return res.status(500).json({
            message: 'No existe el Usuario', 
            code: 500
        });
    })
}

module.exports = userController;