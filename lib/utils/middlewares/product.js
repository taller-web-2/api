'use strict';


function checkBody(req, res, next) {
    if (!req.body) {
        return res.status(500).json({
            message: 'Faltan Datos', 
            code: 400
        });
    }
    return next();
}

module.exports = checkBody;