'use strict'
function validateNewUserEmail(newUser) {
    const mailjet = require('node-mailjet').connect( 
        process.env.MJ_APIKEY_PUBLIC,process.env.MJ_APIKEY_PRIVATE);
    const request = mailjet.post('send', { version: 'v3.1' }).request({
        "Messages": [{
            "From": {
                "Email": 'tallerweb2.unlam@gmail.com',
                "Name": 'Taller web 2',
            },
            "To": [{
                "Email": `${newUser.email}`,
                "Name": `${newUser.firstName}` + ' ' + `${newUser.lastName}`,
            },
        ],

            "TemplateID": 2998748,
            "TemplateLanguage": true,
            "Subject": 'WELCOME ' + `${newUser.firstName}` + ' ' + `${newUser.lastName}` + ', PLEASE ACTIVE YOUR ACCOUNT',
            "Variables": {
                "tokenValidator": `${newUser.tokenValidator.toString()}`
            },
        },
        ],
    })
    request
        .catch(err => {
            console.log("Entra por acá " + err)
        }); 
}

module.exports = validateNewUserEmail;
