'use strict';
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;
const JWT_ISSUER = process.env.JWT_ISSUER;

function extractJwt(req, res, next) {
    const issuers = JWT_ISSUER;
    jwt.verify(
        getJwt(req),
        JWT_SECRET,
        {issuer: issuers},
        (err, decoded) => {
            if (err) {
                return res.status(401).json({
                    code: 'unauthorized',
                    message: 'Unauthorized'
                });
            }
            req.decodedToken = decoded;
            return next();
        }
    );
}

module.exports = extractJwt;
