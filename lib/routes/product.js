const express = require('express');
const router = express.Router();
const productController = require('../controllers/product.Controller');
const checkBody  = require('../utils/middlewares/product')

router.get('/', productController.getTodosProductos);
router.get('/:id', productController.getProducto);
router.post('/newProduct', checkBody, productController.newProducto);

module.exports = router;