'use strict';
const express = require('express');
const router = express.Router();
const User = require('../models/User');
const userController = require('../controllers/user.Controller');
const validateNewUserEmail = require('../utils/email-sender');
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;
const JWT_ISSUER = process.env.JWT_ISSUER;

function createToken(user, expiresIn, issuer) {
    const token = jwt.sign(
        user.toToken(),
        JWT_SECRET,
        {
            expiresIn,
            issuer
        }
    );
    return token;
}

function validation(req, res, next) {
    if (!req.body.email) {
        return res.status(400).json({
            message: 'El email es requerido',
            code: 'missing_params'
        });
    }
    if (!req.body.password) {
        return res.status(400).json({
            message: 'La contraseña es requerida',
            code: 'missing_params'
        });
    }

    return next();
}

router.post('/login', validation, (req, res) => {
    req.body.email = req.body.email.toLowerCase();
    User.findOne({email: req.body.email})
        .then((user) => {
            if (!user || !user.validPassword(req.body.password)) {
                return res.status(400).json({
                    message: 'Los datos no son válidos',
                    code: 'authentication_failed'
                });
            }
            const token = createToken(user, '1d', JWT_ISSUER);
            return res.status(200).json({token});
        })
        .catch((error) => {
            console.error(`findUserError: ${error.message}`);
            return res.status(400).json({
                message: 'Ocurrió un error inesperado',
                code: 'authentication_failed'
            });
        });
});

function validationEmail(req, res, next) {
    if(!((/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/).test(req.body.email))) {
        return res.status(400).json({
            message: 'Los datos no son válidos',
            code: 'signup_failed'
        });
    }
    return next();
}

function checkIfExist(req, res, next) {
    User.countDocuments({email: req.body.email}).then((count) => {
        if (count === 0) {
            next();
            return;
        }
        res.status(400).json({error: 'email already in use!', code: 'email_alredy_used'});
    }).catch((error) => {
        console.error('countUserError: ', error);
    });
}

router.post('/signup', validation, validationEmail, checkIfExist, (req, res) => {
    var newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        address: req.body.address,
        password: req.body.password,
        email: req.body.email.toLowerCase(),
        tokenValidator: Math.floor(100000 + Math.random() * 900000)
    }); 
    return newUser.save()
        .then((userSaved) => {
            validateNewUserEmail(userSaved);
            return res.status(200).send({ 
                code: 200,
                message: 'User Created'
            });
        })
        .catch((error) => {
            console.error('signupError', error.message);
            if(error.message === 'createUser_error') {
                User.deleteOne({email: req.body.email})
                    .then(() => {
                        res.status(400).json({error: 'save user error', code: 'deleting_user_error', status: 'error'});
                    })
                    .catch((err) => {
                        console.error(err.message);
                        res.status(400).json({error: 'save user error', code: 'deleting_user_error_in_catch', status: 'error'});
                    });
            } else {
                res.status(400).json({error: 'save user error', code: 'save_error', status: 'error'});
            }
        });
});

router.post('/validateUser/:tokenValidator', userController.validateUserByToken);

module.exports = router;
