'use strict';
const Products = require('./product');
const Auth = require('./auth');

module.exports = {
    Products,
    Auth
};
