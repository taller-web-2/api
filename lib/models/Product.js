'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    classification: { type: String, required: true },
    price: { type: Number, required: true},
    imageUrl: {type: String,}
}, {
    timestamps: true,
    versionKey: false
})

module.exports = mongoose.model('Product', productSchema);
