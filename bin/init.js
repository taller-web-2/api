'use strict';
require('dotenv').config();
const app = require('../app');

return app.connectMongoose()
    .then(() => {
        const application = app.initialize();
        application.listen(process.env.SERVER_PORT || 3001);
        console.info(`El server está funcionando en el puerto ${process.env.SERVER_PORT}`);
    })
    .catch((error) => {
        console.error('APP STOPPED');
        console.error(error.stack);
        return process.exit(1);
    });
